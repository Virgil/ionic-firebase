export interface Item{

    key?: string;
    name: string;
    description: string;
    price: undefined;
}