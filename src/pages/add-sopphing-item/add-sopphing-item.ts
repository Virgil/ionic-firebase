import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Item } from '../../models/item/item.model';
import { ShoppingListProvider } from '../../providers/shopping-list/shopping-list';

/**
 * Generated class for the AddSopphingItemPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-sopphing-item',
  templateUrl: 'add-sopphing-item.html',
})
export class AddSopphingItemPage {

  item: Item = {
    name: '',
    description: '',
    price: undefined,
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, private shopping: ShoppingListProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddSopphingItemPage');
  }

  addItem(item: Item){
    this.shopping.addItem(item).then(ref => {
      console.log(ref.key);
    })
  }

}
