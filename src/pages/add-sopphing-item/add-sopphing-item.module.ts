import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddSopphingItemPage } from './add-sopphing-item';

@NgModule({
  declarations: [
    AddSopphingItemPage,
  ],
  imports: [
    IonicPageModule.forChild(AddSopphingItemPage),
  ],
})
export class AddSopphingItemPageModule {}
